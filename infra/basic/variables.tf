# ******************************** Common vars
variable "region" {
  description = "AWS region for all recources and providers"
  type        = string
  default     = "us-east-2"
}
variable "azs" {
  description = "AWS availability zones"
  type        = list(string)
  default     = ["us-east-2a", "us-east-2b", "us-east-2c"]
}
variable "env_name" {
  description = "Environment tag"
  type        = string
  default     = "dev"
}
