data "aws_instances" "ec2-pool" {}
data "aws_caller_identity" "current" {}

output "ec2_instance_ids" {
  description = "EC2 instance IDs"
  value = data.aws_instances.ec2-pool.ids
}

output "account_id" {
  value = data.aws_caller_identity.current.account_id
}
