
module "instance_schedule" {
  count    = length(local.instance_ids)

  source = "../modules/ec2_scheduler"

  state       = var.by_schedule

  region = var.region
  account_id = local.account_id
  instance_id = local.instance_ids[count.index]

  #start_schedule = ""
  #stop_schedule = ""
  #local_tz = ""

}



