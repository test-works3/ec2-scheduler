# *************************************** Policy documents
# Assuming to an EventBridge scheduler
data "aws_iam_policy_document" "assume_to_scheduler" {
  version = "2012-10-17"
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["scheduler.amazonaws.com"]
    }
  }
}

# Allow to Start and Stop EC2 instances
data "aws_iam_policy_document" "start_stop_instances" {
  statement {
    actions   = ["ec2:StartInstances", "ec2:StopInstances"]
    effect    = "Allow"
    resources = ["arn:aws:ec2:${var.region}:${var.account_id}:instance/${var.instance_id}"]
  }
}

# *************************************** Policies
resource "aws_iam_policy" "start_stop_instances" {
  name        = "can-start-and-stop-${var.instance_id}"
  description = "Allows to Start and Stop EC2 instances"
  policy      = data.aws_iam_policy_document.start_stop_instances.json
}



# *************************************** Roles 
resource "aws_iam_role" "scheduler_role" {
  name               = "ec2-${var.instance_id}-scheduler-role"
  description        = "Role to start/stop an instance by schedule"
  assume_role_policy = data.aws_iam_policy_document.assume_to_scheduler.json
}

resource "aws_iam_role_policy_attachment" "scheduler" {
  role       = aws_iam_role.scheduler_role.name
  policy_arn = aws_iam_policy.start_stop_instances.arn
}

