data "terraform_remote_state" "base" {
  backend = "s3"
  config = {
    bucket  = "wego-infra-dev-base"
    key     = "terraform.tfstate"
    region  = "us-east-2"
    encrypt = true
  }
}

locals {
  account_id = data.terraform_remote_state.base.outputs.account_id
  instance_ids = data.terraform_remote_state.base.outputs.ec2_instance_ids
}
