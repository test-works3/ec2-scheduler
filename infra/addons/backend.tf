terraform {
  backend "s3" {
    bucket         = "wego-infra-dev-addon"
    key            = "terraform.tfstate"
    region         = "us-east-2"
    encrypt        = true
    dynamodb_table = "terraform-locks-infra-dev-addon-01"
  }
}
/*
aws s3  mb --region "us-east-2" s3://wego-infra-dev-addon
aws dynamodb create-table --table-name terraform-locks-infra-dev-addon-01 \
                          --attribute-definitions AttributeName=LockID,AttributeType=S \
                          --key-schema AttributeName=LockID,KeyType=HASH \
                          --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
                          --region "us-east-2"
*/
