# __generated__ by Terraform
# Please review these resources and move them into your main configuration files.

# __generated__ by Terraform
resource "aws_instance" "inst-1" {
  ami                                  = "ami-07d7e3e669718ab45"
  associate_public_ip_address          = true
  availability_zone                    = "us-east-2a"
  instance_initiated_shutdown_behavior = "stop"
  instance_type                        = "t2.micro"
  key_name                             = "onoff"
  security_groups                      = ["launch-wizard-2"]
  subnet_id                            = "subnet-05275e53d6b55aa6a"
  tags_all = {
    Name = "dev-2"
    Environment = var.env_name
    Terraform = "true"
  }
  vpc_security_group_ids      = ["sg-0cd171919a7f7b0a9"]
}

# __generated__ by Terraform
resource "aws_instance" "inst-0" {
  ami                                  = "ami-07d7e3e669718ab45"
  associate_public_ip_address          = true
  availability_zone                    = "us-east-2a"
  instance_initiated_shutdown_behavior = "stop"
  instance_type                        = "t2.micro"
  key_name                             = "onoff"
  security_groups                      = ["launch-wizard-1"]
  subnet_id                            = "subnet-05275e53d6b55aa6a"
  tags_all = {
    Name = "dev-1"
    Environment = var.env_name
    Terraform = "true"

  }
  vpc_security_group_ids      = ["sg-0722934716ea74512"]
}
