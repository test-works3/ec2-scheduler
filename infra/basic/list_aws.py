import boto3
import argparse

def list_ec2_instances(region):
    ec2 = boto3.client('ec2', region_name=region)
    response = ec2.describe_instances()
    instances = [
        instance['InstanceId'] 
        for reservation in response['Reservations'] 
        for instance in reservation['Instances']
    ]
    return instances

def list_s3_buckets():
    s3 = boto3.client('s3')
    response = s3.list_buckets()
    buckets = [bucket['Name'] for bucket in response['Buckets']]
    return buckets

def list_key_pairs(region):
    ec2 = boto3.client('ec2', region_name=region)
    response = ec2.describe_key_pairs()
    key_pairs = [{"name": key_pair['KeyName'], "pub_key": key_pair['KeyFingerprint']} for key_pair in response['KeyPairs']]
    #key_fingerprints = [key['KeyFingerprint'] for key in response['KeyPairs']]
    return key_pairs

def generate_terraform_config(file_path, ec2_instances, s3_buckets, key_pairs):
    with open(file_path, 'w') as tf_file:
        for index, instance_id in enumerate(ec2_instances):
            tf_file.write(f'import {{\n    to = aws_instance.inst-{index}\n    id = "{instance_id}"\n}}\n\n')
    #    for index, bucket_name in enumerate(s3_buckets):
    #        tf_file.write(f'import {{\n    to = aws_s3_bucket.bucket-{index}\n    id = "{bucket_name}"\n}}\n\n')
    #    for index, key_pair in enumerate(key_pairs):
    #        tf_file.write(f'import {{\n    to = aws_key_pair.key-{index}\n    id = "{key_pair['name']}"\n    #public_key = "{key_pair['pub_key']}"\n}}\n\n')

def main():
    parser = argparse.ArgumentParser(description='Generate Terraform config for AWS resources.')
    parser.add_argument('file_path', help='Path to the Terraform configuration file')
    args = parser.parse_args()
    
    region = 'us-east-2'
    ec2_instances = list_ec2_instances(region)
    s3_buckets = list_s3_buckets()
    key_pairs = list_key_pairs(region)
    
    generate_terraform_config(args.file_path, ec2_instances, s3_buckets, key_pairs)

if __name__ == "__main__":
    main()
