output "start_schedule_id" {
  description = "ID of the schedule to start a ec2 instance"
  value       = aws_scheduler_schedule.start_schedule.id
}

output "stop_schedule_id" {
  description = "ID of the schedule to stop a ec2 instance"
  value       = aws_scheduler_schedule.stop_schedule.id
}