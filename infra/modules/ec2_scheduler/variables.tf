variable "state" {
  description = "Enable or disable the scheduler"
  type        = bool
  default     = true
}

variable "region" {
}

variable "account_id" {
}

variable "instance_id" {
  description = "EC2 instance ID"
  type        = string
}

variable "start_schedule" {
  description = "A cron expression to start the ec2 instance. Example: start everyday at 08am '00 08 * * ? *'"
  type        = string
  default     = "00 08 * * ? *"
}

variable "stop_schedule" {
  description = "A cron expression to stop the ec2 instance. Example: start everyday at 09pm '00 21 * * ? *'"
  type        = string
  default     = "00 21 * * ? *"
}

variable "local_tz" {
  description = "Time Zone string"
  type        = string
  default     = "Europe/Kiev"
}