resource "aws_scheduler_schedule" "start_schedule" {
  name  = "start-schedule-${var.instance_id}"
  state = var.state ? "ENABLED" : "DISABLED"

  schedule_expression          = "cron(${var.start_schedule})"
  schedule_expression_timezone = var.local_tz

  target {
    arn      = "arn:aws:scheduler:::aws-sdk:ec2:startInstances"
    role_arn = aws_iam_role.scheduler_role.arn
    input    = jsonencode({ "InstanceIds" : ["{${var.instance_id}}"] })
  }

  flexible_time_window {
    mode = "OFF"
  }
}

resource "aws_scheduler_schedule" "stop_schedule" {
  name  = "stop-schedule-${var.instance_id}"
  state = var.state ? "ENABLED" : "DISABLED"

  schedule_expression          = "cron(${var.stop_schedule})"
  schedule_expression_timezone = var.local_tz

  target {
    arn      = "arn:aws:scheduler:::aws-sdk:ec2:stopInstances"
    role_arn = aws_iam_role.scheduler_role.arn
    input    = jsonencode({ "InstanceIds" : ["{${var.instance_id}}"] })
  }

  flexible_time_window {
    mode = "OFF"
  }
}